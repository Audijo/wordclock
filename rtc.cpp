#include <WiFi.h>
#include <TimeLib.h>
#include <Wire.h>
#include <NTP.h>
#include <DS3231.h>
#include <ArduinoLog.h>

#include "rtc.h"

WiFiUDP Udp;
NTP ntp(Udp);

DS3231 rtc;
bool h12Flag;
bool pmFlag;

const char* wl_status_to_string(wl_status_t status)
{
	switch (status) {
		case WL_NO_SHIELD: return "WL_NO_SHIELD";
		case WL_IDLE_STATUS: return "WL_IDLE_STATUS";
		case WL_NO_SSID_AVAIL: return "WL_NO_SSID_AVAIL";
		case WL_SCAN_COMPLETED: return "WL_SCAN_COMPLETED";
		case WL_CONNECTED: return "WL_CONNECTED";
		case WL_CONNECT_FAILED: return "WL_CONNECT_FAILED";
		case WL_CONNECTION_LOST: return "WL_CONNECTION_LOST";
		case WL_DISCONNECTED: return "WL_DISCONNECTED";
		default: return "WL_STATUS_UNKNOWN";
	}
}

void rtcSetup()
{
	Log.noticeln("RTC setup");

	// last sunday in march 2:00, timetone +120min (+1 GMT + 1h summertime offset)
	ntp.ruleDST("CEST", Last, Sun, Mar, 2, 120);
	// last sunday in october 3:00, timezone +60min (+1 GMT)
	ntp.ruleSTD("CET", Last, Sun, Oct, 3, 60);
	ntp.begin();

	Wire.begin();
	rtc.setClockMode(true);

	Log.noticeln("RTC ready");
}

void rtcLoop()
{
	ntp.update();

	Log.noticeln("Wifi status: %s", wl_status_to_string(WiFi.status()));
	Log.noticeln("RTC Time   : %d:%d", rtc.getHour(h12Flag, pmFlag),
					   rtc.getMinute());
	Log.noticeln("NTP Time   : %d:%d", ntp.hours(), ntp.minutes());

	int h = ntp.hours();
	if (h > 12) {
		h = h - 12;
	}
	if (h != rtc.getHour(h12Flag, pmFlag)
			|| ntp.minutes() != rtc.getMinute()) {
		rtc.setSecond(ntp.seconds());
		rtc.setMinute(ntp.minutes());
		rtc.setHour(ntp.hours());
		rtc.setDoW(ntp.weekDay());
		rtc.setDate(ntp.day());
		rtc.setMonth(ntp.month());
		rtc.setYear(ntp.year());
		Log.noticeln("RTC set from NTP: %d:%d", ntp.hours(),
							ntp.minutes());
	}
}

uint8_t rtcHour()
{
	return rtc.getHour(h12Flag, pmFlag);
}

uint8_t rtcMinute()
{
	return rtc.getMinute();
}

/*
 *     x x x x x x x x x x x
 *     1 2 3 4 5 6 7 8 9 1 1
 *                       0 1
 *  
 * y1  E S K I S T L F Ü N F
 * y2  Z E H N Z W A N Z I G
 * y3  D R E I V I E R T E L
 * y4  T G N A C H V O R J M
 * y5  H A L B X Z W Ö L F P
 * y6  Z W E I N S I E B E N
 * y7  K D R E I R H F Ü N F
 * y8  E L F N E U N V I E R
 * y9  W A C H T Z E H N R S
 * y10 B S E C H S F M U H R
 */
 
#include "pixel_layout.h"

const int min_ONE[]      = {MIN1};
const int min_TWO[]      = {MIN2};
const int min_THREE[]    = {MIN3};
const int min_FOUR[]     = {MIN4};

const int word_ES[]      = {X1Y1, X2Y1};
const int word_IST[]     = {X4Y1, X5Y1, X6Y1};
const int word_UHR[]     = {X9Y10, X10Y10, X11Y10};

const int word_VOR[]     = {X7Y4, X8Y4, X9Y4};
const int word_NACH[]    = {X3Y4, X4Y4, X5Y4, X6Y4};
const int word_VIERTEL[] = {X5Y3, X6Y3, X7Y3, X8Y3, X9Y3, X10Y3, X11Y3};
const int word_HALB[]    = {X1Y5, X2Y5, X3Y5, X4Y5};

const int word_EIN[]     = {X3Y6, X4Y6, X5Y6};
const int word_EINS[]    = {X3Y6, X4Y6, X5Y6, X6Y6};
const int word_ZWEI[]    = {X1Y6, X2Y6, X3Y6, X4Y6};
const int word_DREI[]    = {X2Y7, X3Y7, X4Y7, X5Y7};
const int word_VIER[]    = {X8Y8, X9Y8, X10Y8, X11Y8};
const int word_FUENF1[]  = {X8Y1, X9Y1, X10Y1, X11Y1};
const int word_FUENF2[]  = {X8Y7, X9Y7, X10Y7, X11Y7};
const int word_SECHS[]   = {X2Y10, X3Y10, X4Y10, X5Y10, X6Y10};
const int word_SIEBEN[]  = {X6Y6, X7Y6, X8Y6, X9Y6, X10Y6, X11Y6};
const int word_ACHT[]    = {X2Y9, X3Y9, X4Y9, X5Y9};
const int word_NEUN[]    = {X4Y8, X5Y8, X6Y8, X7Y8};
const int word_ZEHN1[]   = {X1Y2, X2Y2, X3Y2, X4Y2};
const int word_ZEHN2[]   = {X6Y9, X7Y9, X8Y9, X9Y9};
const int word_ELF[]     = {X1Y8, X2Y8, X3Y8};
const int word_ZWOELF[]  = {X6Y5, X7Y5, X8Y5, X9Y5, X10Y5};
const int word_ZWANZIG[] = {X5Y2, X6Y2, X7Y2, X8Y2, X9Y2, X10Y2, X11Y2};

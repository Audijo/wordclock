all:
	arduino-cli compile -b esp32:esp32:d32 --clean

upload:
	arduino-cli upload -p /dev/ttyUSB0 -b esp32:esp32:d32

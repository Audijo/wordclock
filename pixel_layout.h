/*
 *   0 X
 *     |
 *   1 X
 *     |
 *   2 X--X--X--X--X--X--X--X--X--X 11
 *                                |
 *                                X 12
 *                                |
 *  22 X--X--X--X--X--X--X--X--X--X 13
 *     |
 *  23 X--X--X--X--X--X--X--X--X--X 32
 *                                |
 *  42 X--X--X--X--X--X--X--X--X--X 33
 *     |
 *  43 X--X--X--X--X--X--X--X--X--X 52
 *                                |
 *  62 X--X--X--X--X--X--X--X--X--X 53
 *     |
 *  63 X--X--X--X--X--X--X--X--X--X 72
 *                                |
 *  82 X--X--X--X--X--X--X--X--X--X 73
 *     |
 *  83 X--X--X--X--X--X--X--X--X--X 92
 *                                |
 * 102 X--X--X--X--X--X--X--X--X--X 93
 *     |
 * 103 X
 *     |
 * 104 X--X--X--X--X--X--X--X--X--X 113
 *                                |
 *                                X 114
 *
 * X=11
 * Y=10
 */

#define X1Y1     11
#define X1Y2     10
#define X1Y3      9
#define X1Y4      8
#define X1Y5      7
#define X1Y6      6
#define X1Y7      5
#define X1Y8      4
#define X1Y9      3
#define X1Y10     2

#define X2Y1     13
#define X2Y2     14
#define X2Y3     15
#define X2Y4     16
#define X2Y5     17
#define X2Y6     18
#define X2Y7     19
#define X2Y8     20
#define X2Y9     21
#define X2Y10    22

#define X3Y1     32
#define X3Y2     31
#define X3Y3     30
#define X3Y4     29
#define X3Y5     28
#define X3Y6     27
#define X3Y7     26
#define X3Y8     25
#define X3Y9     24
#define X3Y10    23

#define X4Y1     33
#define X4Y2     34
#define X4Y3     35
#define X4Y4     36
#define X4Y5     37
#define X4Y6     38
#define X4Y7     39
#define X4Y8     40
#define X4Y9     41
#define X4Y10    42

#define X5Y1     52
#define X5Y2     51
#define X5Y3     50
#define X5Y4     49
#define X5Y5     48
#define X5Y6     47
#define X5Y7     46
#define X5Y8     45
#define X5Y9     44
#define X5Y10    43

#define X6Y1     53
#define X6Y2     54
#define X6Y3     55
#define X6Y4     56
#define X6Y5     57
#define X6Y6     58
#define X6Y7     59
#define X6Y8     60
#define X6Y9     61
#define X6Y10    62

#define X7Y1     72
#define X7Y2     71
#define X7Y3     70
#define X7Y4     69
#define X7Y5     68
#define X7Y6     67
#define X7Y7     66
#define X7Y8     65
#define X7Y9     64
#define X7Y10    63

#define X8Y1     73
#define X8Y2     74
#define X8Y3     75
#define X8Y4     76
#define X8Y5     77
#define X8Y6     78
#define X8Y7     79
#define X8Y8     80
#define X8Y9     81
#define X8Y10    82

#define X9Y1     92
#define X9Y2     91
#define X9Y3     90
#define X9Y4     89
#define X9Y5     88
#define X9Y6     87
#define X9Y7     86
#define X9Y8     85
#define X9Y9     84
#define X9Y10    83

#define X10Y1    93
#define X10Y2    94
#define X10Y3    95
#define X10Y4    96
#define X10Y5    97
#define X10Y6    98
#define X10Y7    99
#define X10Y8   100
#define X10Y9   101
#define X10Y10  102

#define X11Y1   113
#define X11Y2   112
#define X11Y3   111
#define X11Y4   110
#define X11Y5   109
#define X11Y6   108
#define X11Y7   107
#define X11Y8   106
#define X11Y9   105
#define X11Y10  104

#define MIN1     12
#define MIN2    114
#define MIN3    103
#define MIN4      1
